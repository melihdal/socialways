import numpy as np
from utils.parse_utils import BIWIParser, create_dataset,FMDParser
import pdb 

#annot_file = '/content/socialways/data/ewap_dataset/seq_eth/obsmat.txt'
#annot_file = 'D:\\Research\\data\ewap_dataset\\seq_eth\\obsmat.txt'
annot_file = 'D:\\Research\\data\customdataset\\circle.txt'
#npz_out_file = '../data-8-12.npz'
npz_out_file = '../FMD-8-12.npz'
#parser = BIWIParser()
parser = FMDParser()

parser.load(annot_file)
pdb.set_trace()
obsvs, preds, times, batches = create_dataset(parser.p_data,
                                              parser.t_data,
                                              range(parser.t_data[0][0], parser.t_data[-1][-1], parser.interval))

np.savez(npz_out_file, obsvs=obsvs, preds=preds, times=times, batches=batches)
